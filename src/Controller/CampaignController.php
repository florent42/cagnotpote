<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{
   
    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {

        
        $campaign = new Campaign();
        // $id = md5(\random_bytes(50));
        
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        $campaign->setName($request->request->get('name'));

        // dd($campaign);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $campaign->setId();
            $em->persist($campaign);
            $em->flush();

            return $this->redirectToRoute('campaign_show', [
                "id" => $campaign -> getId()
            ]);
        }

        return $this->render('campaign/new.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign): Response
    {
        $query = 'SELECT * FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id
        WHERE campaign_id = "'.$campaign->getId() .'"' ;
        
        $statement =  $this->getDoctrine()
                            ->getManager()
                            ->getConnection()
                            ->prepare($query);
                            
        $statement->execute();

        $participantWithParticipations = $statement->fetchAll();

        $totalAmount=0;
        $totalParticipant= count($participantWithParticipations);
        

        for ( $i=0 ; $i <= ($totalParticipant -1); $i++)
        {
            $totalAmount += $participantWithParticipations[$i]["amount"];
        }
        $totalAmount_cent = $totalAmount / 100;
        //valeur en Pourcentage
        $objectif = round(($totalAmount_cent * 100)/ $campaign->getGoal());

        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations','totalAmount', 'totalParticipant', 'objectif'));
    }
     /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
     */
    public function pay(Campaign $campaign): Response
    {

        //Coalesce operator
        $amount_participant = $_GET['amount_participant'] ?? "";
        return $this->render('campaign/pay.html.twig', ['campaign' => $campaign]);
    }

     /**
     * @Route("/{id}/spend", name="campaign_spend", methods="GET|POST")
     */
    public function spend(Campaign $campaign): Response
    {
        return $this->render('campaign/spend.html.twig', ['campaign' => $campaign]);
    }


    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_edit', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }
}
