<?php

namespace App\Controller;

use App\Entity\Spending;
use App\Entity\Participant;
use App\Form\PaymentType;
use App\Form\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class SpendingController extends AbstractController
{
   
    /**
     * @Route("/spend", name="spending_charge", methods="POST")
     */
    public function charge(Request $request): Response
    {

        $amount = (int)$request->request->get("amount") * 100;
           


       //enregistrer un participant
        $participant = new Participant();

        $participant -> setName($request->request->get('name'));
        $participant -> setEmail($request->request->get('email'));
        $participant -> setCampaignId($request->request->get('campaign_id'));
        
            $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

        //enregistrement du paiement qui est dépendant du participant
        $spend = new Spending();

        $spend->setAmount($amount);
        $spend->setParticipant($participant);
        $spend->setLabel($request->request->get("label"));
          
        
            $em = $this->getDoctrine()->getManager();
            $em->persist($spend);
            $em->flush();

            // Redirection vers la fiche campagne 
            return $this->redirectToRoute('campaign_show',[
                "id" => $request->request->get("campaign_id")
            ]); 

            dd("Bravo !");
          
    } 
}
