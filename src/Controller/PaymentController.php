<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Participant;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
   
    /**
     * @Route("/charge", name="payment_charge", methods="POST")
     */
    public function charge(Request $request): Response
    {

        $amount = (int)$request->request->get("amount") * 100;
        //Effectuer le paiement, ne pas continuer la fonction si on échoue
        \Stripe\Stripe::setApiKey('sk_test_8AUQIRuft87AvF54vlHXkfNi');
        $charge = \Stripe\Charge::create(['amount' => $amount, 'currency' => 'eur', 'source' => $request->request->get('stripeToken')]);


       //enregistrer un participant
        $participant = new Participant();

        $participant -> setName($request->request->get('name'));
        $participant -> setEmail($request->request->get('email'));
        $participant -> setCampaignId($request->request->get('campaign_id'));
        
        $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

        //enregistrement du paiement qui est dépendant du participant
        $payment = new Payment();

        $payment->setAmount($amount);

        $payment->setParticipantId($participant->getId());
          
        
            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            // Redirection vers la fiche campagne 
            return $this->redirectToRoute('campaign_show',[
                "id" => $request->request->get("campaign_id")
            ]); 

            dd("Bravo !");
          
    } 
}
